const route = require('express').Router()
const { TeacherController, ClassController, VehiclesController, MoviesController, MoviesTeachersController } = require('../controller')

route.post("/teacher", TeacherController.createTeacher)
route.get("/teacher", TeacherController.getTeachers)
route.get("/teacher/:id", TeacherController.getTeacher)
route.put("/teacher/:id", TeacherController.editTeacher)
route.delete("/teacher/:id", TeacherController.deleteTeacher)

route.post("/class", ClassController.createClass)
route.get("/class", ClassController.getClasses)

route.post("/vehicle", VehiclesController.createVehicle)
route.get("/vehicle", VehiclesController.getVehicles)

route.post("/movie", MoviesController.createMovies)
route.get("/movie", MoviesController.getMovies)

route.post("/moviesTeachers", MoviesTeachersController.createMoviesTeachers)
route.get("/moviesTeachers", MoviesTeachersController.getMoviesTeachers)


module.exports = route

