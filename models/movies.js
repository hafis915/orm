'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movies extends Model {
    static associate(models) {
      // Movies.hasMany(models.MoviesTeachers)
      Movies.belongsToMany(models.Teachers, { through: "MoviesTeachers" })
    }
  };
  Movies.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Movies',
  });
  return Movies;
};