'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MoviesTeachers extends Model {
    static associate(models) {
      MoviesTeachers.belongsTo(models.Movies)
      MoviesTeachers.belongsTo(models.Teachers)
      // MoviesTeachers.hasMany(models.Teachers, { foreignKey: "teachersId" })
      // MoviesTeachers.hasMany(models.Movies, { foreignKey: "moviesId" })
      // define association here
    }
  };
  MoviesTeachers.init({
    MovieId: DataTypes.INTEGER,
    TeacherId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'MoviesTeachers',
  });
  return MoviesTeachers;
};