'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vehicles extends Model {
    static associate(models) {
      Vehicles.belongsTo(models.Teachers, { foreignKey: 'TeacherId' })
    }
  };
  Vehicles.init({
    Name: DataTypes.STRING,
    TeacherId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Vehicles',
  });
  return Vehicles;
};