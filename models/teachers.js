'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teachers extends Model {
    static associate(models) {
      Teachers.hasOne(models.Class)
      Teachers.hasMany(models.Vehicles)
      // Teachers.hasMany(models.MoviesTeachers)
      Teachers.belongsToMany(models.Movies, { through: "MoviesTeachers" })

    }
  };
  Teachers.init({
    Name: DataTypes.STRING,
    Age: DataTypes.INTEGER,
    FullName: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Teachers',
  });
  return Teachers;
};