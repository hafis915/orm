const { Teachers, Class } = require("../models")


class ClassContoller {
  static async createClass(req, res, next) {
    try {
      const { name, TeacherId } = req.body
      const data = await Class.create({
        name, TeacherId
      })
      res.status(201).json(data)
    } catch (error) {

    }
  }

  static async getClasses(req, res, next) {
    try {
      const data = await Class.findAll({
        include: [
          {
            model: Teachers
          }
        ]
      })

      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = ClassContoller