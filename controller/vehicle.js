const { Teachers, Vehicles } = require("../models")


class VehiclesController {
  static async createVehicle(req, res, next) {
    try {
      const { Name, TeacherId } = req.body
      const data = await Vehicles.create({ Name, TeacherId })
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async getVehicles(req, res, next) {
    const data = await Vehicles.findAll({
      include: [
        {
          model: Teachers
        }
      ]
    })

    res.status(200).json(data)
  }
}

module.exports = VehiclesController