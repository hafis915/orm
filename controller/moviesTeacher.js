const { MoviesTeachers, Teachers } = require("../models")


class MoviesTeachersController {
  static async createMoviesTeachers(req, res, next) {
    try {
      const { MovieId, TeacherId } = req.body
      const data = await MoviesTeachers.create({ MovieId, TeacherId })
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMoviesTeachers(req, res, next) {
    try {
      const data = await MoviesTeachers.findAll()
      console.log(data)
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }

  }
}

module.exports = MoviesTeachersController