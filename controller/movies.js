const { Teachers, Movies } = require("../models")


class MoviesController {
  static async createMovies(req, res, next) {
    try {
      const { name } = req.body
      const data = await Movies.create({ name })
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMovies(req, res, next) {
    const data = await Movies.findAll()
    res.status(200).json(data)
  }
}

module.exports = MoviesController