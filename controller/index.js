const TeacherController = require("./teacher")
const ClassController = require("./class")
const VehiclesController = require("./vehicle")
const MoviesController = require("./movies")
const MoviesTeachersController = require("./moviesTeacher")


module.exports = {
  TeacherController,
  ClassController,
  VehiclesController,
  MoviesController,
  MoviesTeachersController
}