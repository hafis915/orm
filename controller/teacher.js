const { Teachers, Vehicles, Class, Movies, MoviesTeachers } = require("../models")


class TeachersController {
  static async createTeacher(req, res, next) {
    try {
      const { Name, Age, FullName } = req.body
      const _payload = {
        Name,
        Age,
        FullName
      }

      const data = await Teachers.create(_payload)
      // console.log(data.datValues.Name, "<<<< DATA CREATE")
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }

  }

  static async getTeachers(req, res, next) {
    try {
      const data = await Teachers.findAll({
        include: [
          {
            model: Vehicles
          },
          {
            model: Class
          },
          {
            model: Movies // relasinya pakai belongsToMany
          }
          // {
          //   model: MoviesTeachers,
          //   include: [
          //     {
          //       model: Movies
          //     }
          //   ]
          // }  // relasinya one to many ke conjunction
        ]
      })
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }

  }

  static async getTeacher(req, res, next) {
    try {
      const id = req.params.id
      const data = await Teachers.findOne({
        where: {
          id: id
        },
        include: [
          {
            model: Vehicles
          },
          {
            model: Class
          },
          {
            model: Movies
          }
        ]
      })
      console.log(data)
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async editTeacher(req, res, next) {
    try {
      const { Name, Age, FullName } = req.body
      const _payload = {
        Name,
        Age,
        FullName
      }
      const id = req.params.id
      const data = await Teachers.update(_payload, {
        where: {
          id
        },
        returning: true
      })
      console.log(data)
      res.status(201).json(data[1][0])
    } catch (error) {
      console.log(error)
    }
  }

  static async deleteTeacher(req, res, next) {
    try {
      const id = req.params.id
      const data = await Teachers.destroy({
        where: {
          id
        }
      })
      if (data) {
        res.status(200).json({
          msg: "Succes to delete"
        })
      } else {
        res.status(404).json({
          msg: "Fail to delete"
        })
      }
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = TeachersController